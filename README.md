This project is based on the starter project from this tutorial: https://www.toptal.com/ios/swift-tutorial-introduction-to-mvvm.
Starter project is in this link: https://bitbucket.org/dino4674/mvvmswiftexample-starter/get/25663cc4ee77.zip.
Customizations include the addition on pods to facilitate the connection of the model into the view via RxSwift.

