//
//  GameScoreboardEditorViewController.swift
//  MVVMSwiftExample
//
//  Created by Dino Bartosak on 25/09/16.
//  Copyright © 2016 Toptal. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

protocol GameScoreboardDelegate {
    
    func update()
    
}
class GameScoreboardEditorViewController: UIViewController {
    
    @IBOutlet weak var homePlayer1View: PlayerScoreboardMoveEditorView!
    @IBOutlet weak var homePlayer2View: PlayerScoreboardMoveEditorView!
    @IBOutlet weak var homePlayer3View: PlayerScoreboardMoveEditorView!

    @IBOutlet weak var awayPlayer1View: PlayerScoreboardMoveEditorView!
    @IBOutlet weak var awayPlayer2View: PlayerScoreboardMoveEditorView!
    @IBOutlet weak var awayPlayer3View: PlayerScoreboardMoveEditorView!
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var pauseButton: UIButton!
    
    @IBOutlet weak var homeTeamNameLabel: UILabel!
    @IBOutlet weak var awayTeamNameLabel: UILabel!
    
    @IBOutlet weak var scoreLabel: UILabel!
    
    var homeScoreObservable: Observable<[PlayerScoreboardMoveEditorViewModel]>? = nil
    
    var viewModel: GameScoreboardEditorViewModel? {
        didSet {
            fillUI()
        }
    }
    
    let isRunning = BehaviorRelay(value: true)
    let isFinished = BehaviorRelay(value: true)
    var disposable: Disposable?
    var disposeBag: DisposeBag?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        styleUI()
        fillUI()
    }
    
    // MARK: Button Action
    
    @IBAction func pauseButtonPress(_ sender: AnyObject) {


        if let isPaused = viewModel?.isPaused {
            if (isPaused) {
                disposable = isRunning.asObservable()
                    .flatMapLatest {  isRunning in
                        isRunning ? Observable<Int>.interval(0.001, scheduler: MainScheduler.instance) : .empty()
                    }
                    //.flatMapWithIndex { (int, index) in Observable.just(index) }
                    .map{int in GameScoreboardEditorViewModelFromGame.timeFormatted(totalMillis: int) }
                    .bind(to: timeLabel.rx.text)
                
            } else {
                if let disposed = self.disposable {
                    disposed.disposed(by: disposeBag!)
                }
            }
        }
            
        viewModel?.togglePause()

        
    }
    
    // MARK: Private

    fileprivate func styleUI() {
        self.view.backgroundColor = UIColor.backgroundColor
        self.scoreLabel.textColor = UIColor.scoreColor
        self.homeTeamNameLabel.textColor = UIColor.textColor
        self.awayTeamNameLabel.textColor = UIColor.textColor
        self.timeLabel.textColor = UIColor.textColor
    }
    
    fileprivate func fillUI() {
        if !isViewLoaded {
            return
        }
        
        guard let viewModel = viewModel else {
            return
        }
        
        // we are sure here that we have all the setup done
        
        self.homeTeamNameLabel.text = viewModel.homeTeam
        self.awayTeamNameLabel.text = viewModel.awayTeam
        
        self.scoreLabel.text = viewModel.score
        self.timeLabel.text = viewModel.time
        
        let title: String = viewModel.isPaused ? "Start" : "Pause"
        self.pauseButton.setTitle(title, for: .normal)
        
        viewModel.homePlayers[0].delegate = self
        viewModel.homePlayers[1].delegate = self
        viewModel.homePlayers[2].delegate = self
        
        homePlayer1View.viewModel = viewModel.homePlayers[0]
        homePlayer2View.viewModel = viewModel.homePlayers[1]
        homePlayer3View.viewModel = viewModel.homePlayers[2]

        viewModel.awayPlayers[0].delegate = self
        viewModel.awayPlayers[1].delegate = self
        viewModel.awayPlayers[2].delegate = self
        
        awayPlayer1View.viewModel = viewModel.awayPlayers[0]
        awayPlayer2View.viewModel = viewModel.awayPlayers[1]
        awayPlayer3View.viewModel = viewModel.awayPlayers[2]

        viewModel.isFinished.asObservable().subscribe({ _ in
            let alert = UIAlertController(title: "Game has ended.", message: nil, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Done", style: .default))
            self.present(alert, animated: true, completion: {
                self.viewModel = GameScoreboardEditorViewModelFromGame(withGame: InMemoryGameLibrary().allGames().first ?? Game.init(homeTeam: Team.init(name: "Ginebra", identifier: "BGK"), awayTeam: Team.init(name: "San Miguel", identifier: "SMB"), identifier: "PBA"))
            })
        }).disposed(by: DisposeBag())
    }

}

extension GameScoreboardEditorViewController: GameScoreboardDelegate {

    func update() {
        viewModel?.setScore()
        
        self.scoreLabel.text = viewModel?.score
        //self.timeLabel.text = viewModel?.time
        
    }
    
    
}
