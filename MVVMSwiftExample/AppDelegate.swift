//
//  AppDelegate.swift
//  MVVMSwiftExample
//
//  Created by Dino Bartosak on 25/09/16.
//  Copyright © 2016 Toptal. All rights reserved.
//

import UIKit
import Swinject
import SwinjectStoryboard

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var container: Assembler = Assembler()
    
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // SETUP WINDOW
        window = UIWindow(frame: UIScreen.main.bounds)
        
        // SETUP OBJECT GRAPH
        Container.loggingFunction = nil  // disable since doesn't work with storyboard plugin
        container.apply(assemblies: [
            AppAssembly()
            ])
        
        
        
//        // SETUP ROOT VIEW CONTROLLER
        let storyboard = SwinjectStoryboard.create(name: "Main", bundle: nil, container: container.resolver)
        let initialViewController = storyboard.instantiateInitialViewController()!
        window!.rootViewController = initialViewController
        window!.makeKeyAndVisible()
        
        return true
    }
    
}

