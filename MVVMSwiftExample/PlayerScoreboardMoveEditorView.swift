//
//  PlayerScoreboardMoveEditorView.swift
//  MVVMSwiftExample
//
//  Created by Dino Bartosak on 26/09/16.
//  Copyright © 2016 Toptal. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class PlayerScoreboardMoveEditorView: UIView {
    
    @IBOutlet weak var onePointCountLabel: UILabel!
    @IBOutlet weak var twoPointCountLabel: UILabel!
    @IBOutlet weak var assistCountLabel: UILabel!
    @IBOutlet weak var reboundCountLabel: UILabel!
    @IBOutlet weak var foulCountLabel: UILabel!
    @IBOutlet weak var threePointLabel: UILabel!
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var onePointButton: UIButton!
    @IBOutlet weak var twoPointsButton: UIButton!
    @IBOutlet weak var assistButton: UIButton!
    @IBOutlet weak var reboundButton: UIButton!
    @IBOutlet weak var foulButton: UIButton!
    @IBOutlet weak var threePointsButton: UIButton!
    
    fileprivate weak var playerNibView: UIView!
    var viewModel: PlayerScoreboardMoveEditorViewModel? {
        didSet {
            fillUI()
        }
    }
    
    var disposeBag: DisposeBag?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        let playerView: UIView = UINib.loadPlayerScoreboardMoveEditorView(self)
        self.addSubview(playerView)
        self.playerNibView = playerView
        
        styleUI()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        playerNibView.frame = self.bounds
    }
    
    
    // MARK: Private
    
    fileprivate func styleUI() {
        self.layer.cornerRadius = 5.0
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.borderColor.cgColor
        self.clipsToBounds = true
    
        self.backgroundColor = UIColor.playerBackgroundColor
        
        styleActionButton(onePointButton)
        styleActionButton(twoPointsButton)
        styleActionButton(assistButton)
        styleActionButton(threePointsButton)
        styleActionButton(reboundButton)
        styleActionButton(foulButton)
    }
    
    fileprivate func styleActionButton(_ button: UIButton) {
        button.setTitleColor(UIColor.scoreColor, for: UIControlState())
        button.layer.cornerRadius = button.bounds.size.width / 2.0
        button.layer.borderWidth = 1.0
        button.layer.borderColor = UIColor.brightPlayerBackgroundColor.cgColor
        button.backgroundColor = UIColor.brightPlayerBackgroundColor
    }
    
    fileprivate func fillUI() {
        guard let viewModel = viewModel else {
            return
        }
        
        self.name.text = viewModel.playerName
        
        
        
        self.onePointButton.rx.tap.subscribe(onNext: { query in
            viewModel.onePointMove()
            self.update()
        }, onCompleted: {
            print("completed")
        }).disposed(by: DisposeBag())

        self.foulButton.rx.tap.subscribe(onNext: { query in
            viewModel.foulMove()
            self.update()
        }, onCompleted: {
            print("completed")
        }).disposed(by: DisposeBag())
        
        
        self.self.threePointsButton.rx.tap.subscribe(onNext: { query in
            viewModel.threePointsMove()
            self.update()
        }, onCompleted: {
            print("completed")
        }).disposed(by: DisposeBag())

        self.twoPointsButton.rx.tap.subscribe(onNext: { query in
            viewModel.twoPointsMove()
            self.update()
        }, onCompleted: {
            print("completed")
        }).disposed(by: DisposeBag())

        self.assistButton.rx.tap.subscribe(onNext: { query in
            viewModel.assistMove()
            self.update()
        }, onCompleted: {
            print("completed")
        }).disposed(by: DisposeBag())

        self.reboundButton.rx.tap.subscribe(onNext: { query in
            viewModel.reboundMove()
            self.update()
        }, onCompleted: {
            print("completed")
        }).disposed(by: DisposeBag())
               
    }
    
    func update() {
        self.onePointCountLabel.text = viewModel?.onePointMoveCount
        self.twoPointCountLabel.text = viewModel?.twoPointMoveCount
        self.assistCountLabel.text = viewModel?.assistMoveCount
        self.reboundCountLabel.text = viewModel?.reboundMoveCount
        self.foulCountLabel.text = viewModel?.foulMoveCount
    }
    
    
}
