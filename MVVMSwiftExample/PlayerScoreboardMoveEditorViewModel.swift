//
//  PlayerScoreboardMoveEditorViewModel.swift
//  MVVMSwiftExample
//
//  Created by RJ Balandra on 12/11/2018.
//  Copyright © 2018 Toptal. All rights reserved.
//

import Foundation

protocol PlayerScoreboardMoveEditorViewModel: class {
    var playerName: String { get }
    var delegate: GameScoreboardDelegate? { get set }
    
    var onePointMoveCount: String { get }
    var twoPointMoveCount: String { get }
    var threePointMoveCount: String { get }
    var assistMoveCount: String { get }
    var reboundMoveCount: String { get }
    var foulMoveCount: String { get }
    
    func onePointMove()
    func twoPointsMove()
    func threePointsMove()
    func assistMove()
    func reboundMove()
    func foulMove()
    
}
