//
//  GameScoreboardEditorViewModel.swift
//  MVVMSwiftExample
//
//  Created by RJ Balandra on 12/11/2018.
//  Copyright © 2018 Toptal. All rights reserved.
//

import Foundation
import RxSwift

protocol GameScoreboardEditorViewModel {

    var homeTeam: String { get }
    var awayTeam: String { get }
    var time: String { get }
    var score: String { get }
    var isFinished: Variable<Bool> { get }
    
    var homePlayers: [PlayerScoreboardMoveEditorViewModel] { get }
    var awayPlayers: [PlayerScoreboardMoveEditorViewModel] { get }

    var isPaused: Bool { get }
    func togglePause()
    func setScore()
}
