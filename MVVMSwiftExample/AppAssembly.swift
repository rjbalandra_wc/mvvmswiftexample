//
//  AppAssembly.swift
//  MVVMSwiftExample
//
//  Created by RJ Balandra on 13/11/2018.
//  Copyright © 2018 Toptal. All rights reserved.
//

import Foundation
import RealmSwift
import Swinject
import SwinjectStoryboard
import RxSwift
import UIKit

class AppAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.register(GameLibrary.self, factory: { _ in
            InMemoryGameLibrary()
        })
        
        // MARK: View Controllers
        container.storyboardInitCompleted(HomeViewController.self) { resolver, controller in
            controller.gameLibrary = container.resolve(GameLibrary.self)
        }
        
        container.storyboardInitCompleted(GameScoreboardEditorViewController.self) { resolver, controller in
            controller.disposeBag = resolver.resolve(DisposeBag.self)
        }
        
        container.register(DisposeBag.self, factory: { _ in
            return DisposeBag()
        })
        
        // MARK: Realm
        // swiftlint:disable force_try
        container.register(Realm.self) { _ in
            try! Realm()
        }
        // swiftlint:enable force_try
        
//        container.register(Game.self, factory: { resolver in
//            
//            })
    }
    
}
