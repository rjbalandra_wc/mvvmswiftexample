//
//  MVVMSwiftExampleBase.swift
//  MVVMSwiftExampleTests
//
//  Created by RJ Balandra on 13/11/2018.
//  Copyright © 2018 Toptal. All rights reserved.
//

import XCTest
@testable import MVVMSwiftExample

class MVVMSwiftExampleBase: XCTestCase {
    
    var app = XCUIApplication()

    let homeTeam: Team = Team(name: "Spurs", identifier: UUID().uuidString)
    let awayTeam: Team = Team(name: "Warriors", identifier: UUID().uuidString)

    override func setUp() {
        super.setUp()
        continueAfterFailure = false

        homeTeam.addPlayer(Player(name: "TD", identifier: UUID().uuidString))
        homeTeam.addPlayer(Player(name: "Manu", identifier: UUID().uuidString))
        homeTeam.addPlayer(Player(name: "TP", identifier: UUID().uuidString))
        awayTeam.addPlayer(Player(name: "Curry", identifier: UUID().uuidString))
        awayTeam.addPlayer(Player(name: "KD", identifier: UUID().uuidString))
        awayTeam.addPlayer(Player(name: "Klay Thompson", identifier: UUID().uuidString))

    }
    
    override func tearDown() {
        super.tearDown()
    }

}
