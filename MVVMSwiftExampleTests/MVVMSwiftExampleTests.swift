//
//  MVVMSwiftExampleTests.swift
//  MVVMSwiftExampleTests
//
//  Created by Dino Bartosak on 25/09/16.
//  Copyright © 2016 Toptal. All rights reserved.
//

import XCTest
@testable import MVVMSwiftExample

class MVVMSwiftExampleTests: XCTestCase {
   
    let homeTeam: Team = Team(name: "Spurs", identifier: UUID().uuidString)
    let awayTeam: Team = Team(name: "Warriors", identifier: UUID().uuidString)
    
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        homeTeam.addPlayer(Player(name: "TD", identifier: UUID().uuidString))
        homeTeam.addPlayer(Player(name: "Manu", identifier: UUID().uuidString))
        homeTeam.addPlayer(Player(name: "TP", identifier: UUID().uuidString))
        awayTeam.addPlayer(Player(name: "Curry", identifier: UUID().uuidString))
        awayTeam.addPlayer(Player(name: "KD", identifier: UUID().uuidString))
        awayTeam.addPlayer(Player(name: "Klay Thompson", identifier: UUID().uuidString))

        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testPlayerExistence() {
        let game: Game = Game(homeTeam: homeTeam, awayTeam: awayTeam, identifier: UUID().uuidString)
        let oldScore = game.homeTeamScore
        game.addPlayerMove(.onePoint, for: Player(name: "TDDD", identifier: UUID().uuidString))
        game.addPlayerMove(.onePoint, for: homeTeam.players.first ?? Player(name: "TD", identifier: UUID().uuidString))
        XCTAssertFalse(game.homeTeamScore == 0)
        XCTAssertTrue(game.awayTeamScore == 0)
        
    }
    
    func test1PointMove() {
        let game: Game = Game(homeTeam: homeTeam, awayTeam: awayTeam, identifier: UUID().uuidString)
        let oldScore = game.homeTeamScore
        game.addPlayerMove(.onePoint, for: homeTeam.players.first ?? Player(name: "TD", identifier: UUID().uuidString))
        XCTAssertTrue(oldScore < game.homeTeamScore)
    }
    
    func test2PointMove() {
        let game: Game = Game(homeTeam: homeTeam, awayTeam: awayTeam, identifier: UUID().uuidString)
        let oldScore = game.homeTeamScore
        game.addPlayerMove(.twoPoints, for: homeTeam.players.first ?? Player(name: "TD", identifier: UUID().uuidString))
        XCTAssertTrue(oldScore < game.homeTeamScore)
    }
    
    func test3PointMove() {
        let game: Game = Game(homeTeam: homeTeam, awayTeam: awayTeam, identifier: UUID().uuidString)
        let oldScore = game.homeTeamScore
        game.addPlayerMove(.threePoints, for: homeTeam.players.first ?? Player(name: "TD", identifier: UUID().uuidString))
        XCTAssertTrue(oldScore < game.homeTeamScore)
    }
    
    func testAssist() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let game: Game = Game(homeTeam: homeTeam, awayTeam: awayTeam, identifier: UUID().uuidString)
        game.addPlayerMove(.assist, for: homeTeam.players.first ?? Player(name: "TD", identifier: UUID().uuidString))
        XCTAssertTrue(game.homeTeamScore == game.awayTeamScore)
    }
    
    func testWinningScoreChange() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let game: Game = Game(homeTeam: homeTeam, awayTeam: awayTeam, identifier: UUID().uuidString)
        let oldWinScore = game.matchWinningScore
        game.setWinningScore(score: 50)
        XCTAssertTrue(oldWinScore != game.matchWinningScore)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
