//
//  MVVMSwiftExampleUITestsExtension.swift
//  MVVMSwiftExampleUITests
//
//  Created by RJ Balandra on 13/11/2018.
//  Copyright © 2018 Toptal. All rights reserved.
//

@testable import MVVMSwiftExample
import XCTest

extension MVVMSwiftExampleUITests {

    func givenILaunchedAnApp() {
        XCTContext.runActivity(named: "Given I Launched an App")  { _ in
            XCUIApplication().launch()
        }
    }

    func iTouchedOnePoint() {
        let element2 = app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element(boundBy: 0).children(matching: .other).element(boundBy: 0)
        
        let button = element2.buttons["1"]
        XCTAssertTrue(button.isEnabled)
        button.tap()
        
    }

    func iTouchedTwoPoint() {
        let element2 = app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element(boundBy: 0).children(matching: .other).element(boundBy: 0)
        
        let button2 = element2.buttons["2"]
        XCTAssertTrue(button2.isEnabled)

    }

    func iTouchedThreePoint() {
        let element2 = app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element(boundBy: 0).children(matching: .other).element(boundBy: 0)
        
        let button3 = element2.buttons["3"]
        XCTAssertTrue(button3.isEnabled)

    }

    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let element2 = app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element(boundBy: 0).children(matching: .other).element(boundBy: 0)
       
        
        let button4 = element2.buttons["Ast"]
        XCTAssertTrue(button4.isEnabled)
        
        let button5 = element2.buttons["Reb"]
        XCTAssertTrue(button5.isEnabled)
        
        let button6 = element2.buttons["Fl"]
        XCTAssertTrue(button6.isEnabled)
        
        let label1 = element2.staticTexts.element(matching:.any, identifier: "1label")
        XCTAssertTrue(label1.isEnabled)
        
        let label2 = element2.staticTexts.element(matching:.any, identifier: "2label")
        XCTAssertTrue(label2.isEnabled)
        
        let label3 = element2.staticTexts.element(matching:.any, identifier: "3label")
        XCTAssertTrue(label3.isEnabled)
        
        let label4 = element2.staticTexts.element(matching:.any, identifier: "alabel")
        XCTAssertTrue(label4.isEnabled)
        
        let label5 = element2.staticTexts.element(matching:.any, identifier: "rlabel")
        XCTAssertTrue(label5.isEnabled)
        
        let label6 = element2.staticTexts.element(matching:.any, identifier: "flabel")
        XCTAssertTrue(label6.isEnabled)
        
        
    }
    
}
