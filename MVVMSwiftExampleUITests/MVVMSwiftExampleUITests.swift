//
//  MVVMSwiftExampleUITests.swift
//  MVVMSwiftExampleUITests
//
//  Created by Dino Bartosak on 25/09/16.
//  Copyright © 2016 Toptal. All rights reserved.
//


import XCTest

class MVVMSwiftExampleUITests: XCTestCase {
    
    var app =  XCUIApplication()
    
    let defaultLaunchArguments: [[String]] = {
        let launchArguments: [[String]] = [["-StartFromCleanState", "YES"]]
        return launchArguments
    }()

    func launchApp(with launchArguments: [[String]] = []) {
        (defaultLaunchArguments + launchArguments).forEach { app.launchArguments += $0 }
        //app.launch()
    }
    
    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        //launchApp(with: defaultLaunchArguments)
    }
    
    override func tearDown() {
        //app.terminate()
        super.tearDown()
    }
    
}
    
